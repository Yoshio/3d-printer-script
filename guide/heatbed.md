---
title: Heizbett
layout: home
parent: Anleitung
nav_order: 2

image: /assets/images/cover/2.jpg
---

# Heatbed und Verbindung

![Komplette Y Achse]({{site.baseurl}}/assets/images/guide/heatbed/completed.png)

Dieses Foto zeigt das fertige Heatbed und die fertige Montage.

* * *

### Was wir für das Heatbed und die Befestigung brauchen...

# Bauteile

| Anzahl | Bauteil                              |
| :----- | :----------------------------------- |
| 1x     | Heatbed Cover (besteht aus 2 Teilen) |
| 1x     | Y-Motorhalter (besteht aus 2 Teilen) |
| 1x     | Y-Riemenspanner                      |
| 3x     | Y-Lagerblöcke                        |
| 4x     | Y-Wellenhalter Oben                  |
| 4x     | Y-Wellenhalter Unten                 |

# Schrauben

| Anzahl | Schrauben                         |
| :----- | :-------------------------------- |
| 2x     | M3x10                             |
| 12x    | M3x12                             |
| 1x     | M3x22                             |
| 2x     | M3x30                             |
| 2x     | M3x40                             |
| 6x     | M3 Nyloc (selbstsichernde) Mutter |
| 6x     | M3 Gleitmutter                    |
| 7x     | M6x12                             |
| 8x     | M6x14                             |
| 15x    | M6 Hammermutter                   |

![Komponenten]({{site.baseurl}}/assets/images/guide/heatbed/components.png)

![Komponenten Teil2]({{site.baseurl}}/assets/images/guide/heatbed/components2.png)

-   1x Carriage
-   1x Heatbed
-   3x Linearlager
-   1x Springsheet

* * *

### Der Zusammenbau

-   Linearlager einpressen

-   Dabei unbedingt auf die Ausrichtung achten!
-   Dazu auf das Foto achten - eine Reihe mit Kugeln im Lager muss nachher auf der Welle liegen.

![Ausrichtung des Linearlagers]({{site.baseurl}}/assets/images/guide/heatbed/linearbearings.jpg)

![Gleitmutter einpressen]({{site.baseurl}}/assets/images/guide/heatbed/slidenut.jpg)

-   Gleitmutter seitlich an beiden Seiten einpressen

-   Gegebenenfalls mit einem schmalen Gegenstand einpressen, sodass das Gewinde der Gleitmuttern und der Löcher überlappen.

-   Selbstsichernde Mutter einpressen.
-   Mit einer Schraube (M3x12) festschrauben.
-   Das ganze für alle 3 Teile wiederholen.

![Schrauben]({{site.baseurl}}/assets/images/guide/heatbed/screw.jpg)

![Montage des Carriage]({{site.baseurl}}/assets/images/guide/heatbed/carriage_assembly.jpg)

-   Carriage wie in Abbildung gezeigt vor sich legen.
-   Die Lagerblöcke am Carriage mit zwei Schrauben (M3x12) fixieren aber nicht festziehen.

-   Riemenhalter mit zwei Schrauben (M3x10) anschrauben.
-   Danach das Carriage zur Seite legen und die Y-Achse zur Hand nehmen.

![Riemenhalter]({{site.baseurl}}/assets/images/guide/heatbed/beltholder.jpg)

![Wellenhalter einlegen]({{site.baseurl}}/assets/images/guide/heatbed/assembly-y.jpg)

-   Wellenhalter (Unten) in die langen Profile einsetzen, mit den Wellenöffnungen zueinander (nach innen zeigend).
-   Die Wellen (360mm) in die Wellenhalter einstecken und mit der Lehre auf einer Seite, wie im Bild zu sehen, ausrichten. Die Lehre entlang der Welle schieben um den gleichen Abstand zum Rahmen zu garantieren.
-   Die Wellenhalter mit jeweils einer Schraube (M6x12) und Hammermutter leicht fixieren, sodass sie sich nicht mehr verschieben lassen.

-   Die zweite Lehre zur Hand nehmen, um den Abstand zur ersten Welle zu bestimmen. Lehre auflegen und Wellenhalter auf einer Seite mit einer Schraube fixieren, Lehre entlang der Wellen führen, um einen gleichmäßigen Abstand zu garantieren, dann den zweiten Wellenhalter fixieren.
-   Die Wellen entfernen und Schrauben (M6x12) durch die Mitte der Wellenhalter führen und mit Hammermutter festschrauben. Andere Schrauben entfernen.

![Welle mit Lehre]({{site.baseurl}}/assets/images/guide/heatbed/assembly-y2.jpg)

![Wellenmontage]({{site.baseurl}}/assets/images/guide/heatbed/assembly-y-completed.jpg)

-   Wellen durch die Lager der Lagerblöcke vom Carriage führen.
-   In die Wellenhalter einführen und Deckel für den Wellenhalter aufsetzen und festschrauben (M6x12).

-   Dabei darauf achten, dass die Seite mit den 2 Linearlagern die linke Seite sein muss.

-   Als nächstes montieren wir den Motor.

-   Dabei müsst ihr darauf achten, dass der Motor hinten an der Y-Achse montiert werden muss.

-   Dazu setzen wir als erstes den Pulley auf den Motor und schrauben diesen an.

-   Achtet dabei das ihr einen kleinen Abstand zwischen Motor und Pulley lasst.

-   Diesen müssen wir zunächst mit drei M3x12 Schrauben an den Halter schrauben. (Wie in der Abbildung zu erkennen)
-   Anschließend muss dieser mit zwei M6x12 Schraube an die Y-Achse angebracht werden (mittig zwischen die Wellenhalter).

![Motormontage]({{site.baseurl}}/assets/images/guide/heatbed/motor.jpg)

![Motormontage]({{site.baseurl}}/assets/images/guide/heatbed/motorsupport.jpg)

-   Jetzt stützen wir den Motor noch ein wenig.
-   Die Unterstützung bringen wir mit zwei M3x40 Schrauben an.

-   Nun bauen wir die Umlenkrolle zusammen.
-   Dazu pressen wir 2 selbstsichernde Muttern ein.
-   Anschließend Schrauben wir das ganze mit zwei M3x30 Schrauben zusammen (Aber nicht fest ziehen).
-   Danach schrauben wir die Umlenkrolle mit einer M3x22 Schraube ein.
-   Zum Schluss befestigen wir die Umlenkrolle an der vom Motor gegenüberliegenen Seite mit einer Hammermutter und einer M6x12 Schraube.

![Motormontage]({{site.baseurl}}/assets/images/guide/heatbed/construct-pulley.jpg)

![Riemen in Halter einlegen]({{site.baseurl}}/assets/images/guide/heatbed/belt.jpg)

-   Nun müssen wir den Riemen einlegen. (Genauso wie es in der Abbildung zu erkennen ist)

-   Danach muss der Riemenhalter zusammengesetzt werden.
-   Dazu pressen wir eine selbstsichernde Mutter ein, und schrauben die Führungsrolle fest.
-   Anschließend Schrauben wir den Riemenhalter an die Y-Achse, und legen den Riemen um den Motor und durch den Halter. (Wie in der Abbildung zu erkennen)

![Riemen in System einlegen]({{site.baseurl}}/assets/images/guide/heatbed/beltholder-y.jpg)

![Riemen spannen]({{site.baseurl}}/assets/images/guide/heatbed/tighten-belt.jpg)

-   Jetzt können wir den Riemen Spannen.

-   Jetzt muss das Heatbed angebracht werden.

-   Dazu wird das Heatbed mit 9 Schrauben befestigt.
-   Anfangen muss man mit der mittleren Schraube.
-   Dieses Schraubloch wird mit einem Abstandshalter versehen, und im Anschluss verschraubt.
-   Die restlichen Abstandhalter müssen vorsichtig mit einer Spitzzange zugefügt werden.

![Riemen in System einlegen]({{site.baseurl}}/assets/images/guide/heatbed/heatbed-assembly.jpg)

![Riemen spannen]({{site.baseurl}}/assets/images/guide/heatbed/springsheet.png)

-   Im Anschluss wird das Springsheet montiert.

-   Da die Oberfläche des Heatbeds magnetisch ist, muss dieses nur korrekt aufgelegt werden.

-   WICHTIG! Bitte das Springsheet die gesamte Zeit auf dem Heatbed lassen!
    {: .text-red-100 }

Das Heatbed ist somit fertig montiert.
