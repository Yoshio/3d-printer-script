---
title: Extruder
layout: home
parent: Anleitung
nav_order: 3

image: /assets/images/cover/3.jpg
---

Der Extruder
============

* * *

![Extruder]({{site.baseurl}}/assets/images/guide/extruder/extruder.jpg)

Schon fertig
============

Der Extruder wurde bereits für euch zusammengebaut.
