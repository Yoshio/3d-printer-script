---
title: Elektronik
layout: home
parent: Anleitung
nav_order: 7

image: /assets/images/cover/7.jpg
---

# Die Elektronik

![Elektronik]({{site.baseurl}}/assets/images/guide/electronic/completed.jpg)

Jetzt geht es an die Elektrik.

* * *

### Was wir für die Elektrik brauchen...

# Bauteile

| Anzahl | Bauteil     |
| :----- | :-----------|
| 1x     | Gehäuse     |
| 1x     | Einsy Board |

# Schrauben

| Anzahl  | Schrauben        |
| :------ | :--------------- |
| 10x     | M3 Muttern       |
| 14x     | M6x10            |
| 4x      | M6x12            |
| 4x      | M3 Gleitmuttern  |
| 4x      | M6 Hammermuttern |

![Komponenten]({{site.baseurl}}/assets/images/guide/electronic/components.jpg)

* * *

### Der Zusammenbau

![Gehäuse an Rahmen]({{site.baseurl}}/assets/images/guide/electronic/fixhousing.jpg)

-   Das Boardgehäuse wird mit zwei M6x12 und zwei Hammermuttern an der linken Seite des Rahmens befestigen.

-   Es sollte ca. 5 cm Platz zwischen Gehäuse und Rahmen gelassen werden.

-   Vier M3 Muttern in die Rückseite des Gehäuses einpressen.

![Vier M3 Muttern einpressen]({{site.baseurl}}/assets/images/guide/electronic/screwfix.jpg)

![Board ins Gehäuse einsetzen]({{site.baseurl}}/assets/images/guide/electronic/fixboard.jpg)

-   Board in das Gehäuse einsetzen und mit M3x10 Schrauben festschrauben.

-   Kabel vom Motor der X-Achse zum Gehäuse führen und in die Öffnung legen.

![Kabel ins Gehäuse legen]({{site.baseurl}}/assets/images/guide/electronic/completed.jpg)

![Board ins Gehäuse einsetzen]({{site.baseurl}}/assets/images/guide/electronic/fixcable.jpg)

-   Kabel mit der Abdeckkappe fixieren, dafür werden jeweils zwei M3x10 Schrauben und M3 Muttern benötigt.
-   Diesen Schritt für die Leitungen des Extruders und des Heizbetts wiederholen.

-   Stecker nach Abbildung stecken.
-   Anschließend 4 Gleitmuttern in das Gehäuse einsetzen und den Deckel mit 4 M3x10 Schrauben verschließen.

![Kabel anschließen]({{site.baseurl}}/assets/images/guide/electronic/wireing.jpg)

![Netzteil anbringen]({{site.baseurl}}/assets/images/guide/electronic/powersupply.jpg)

-   Netzteil mit zwei M6x12 Schrauben und Hammermuttern befestigen.

Somit ist die Elektrik fertig. Das Gerät kann nun in Betrieb genommen werden.
