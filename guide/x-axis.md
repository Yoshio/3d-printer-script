---
title: X-Achse
layout: home
parent: Anleitung
nav_order: 4

image: /assets/images/cover/4.jpg
---

# Die X-Achse

![Komplette X Achse]({{site.baseurl}}/assets/images/guide/x-axis/completed.jpg)

Dieses Foto zeigt die fertige X-Achse.

* * *

### Was wir für die X-Achse brauchen...


# Bauteile

| Anzahl | Bauteil                      |
| :----- | :--------------------------- |
| 7x     | Linearlager                  |
| 2x     | Wellen                       |
| 1x     | Motorhalter                  |
| 1x     | Kabelführungskappe           |
| 1x     | Idler                        |
| 1x     | Gurtspanner                  |
| 1x     | Pulley für Gurtspanner       |
| 1x     | Bolzen / Pin für Gurtspanner |
| 2x     | Trapezgewindespindel         |

# Schrauben

| Anzahl | Schrauben                         |
| :----- | :-------------------------------- |
| 4x     | M3x10                             |
| 1x     | M3x12                             |
| 2x     | M3x14                             |
| 9x     | M3x16                             |
| 1x     | M3x20                             |
| 4x     | M3 Mutter                         |
| 8x     | M3 Nyloc (selbstsichernde) Mutter |
| 2x     | M3 Gleitmutter                    |
| 7x     | M3 Unterlegscheiben               |

## TODO: Add the 2 Screws for Motor Holder Cable Cap
{: .text-red-100 }

![Komponenten]({{site.baseurl}}/assets/images/guide/x-axis/components.jpg)

* * *

### Der Zusammenbau

![Einpressen einer Mutter]({{site.baseurl}}/assets/images/guide/x-axis/selfassuredmother1.jpg)

-   Selbstsichernde Muttern (M3) in die vorgesehenen Öffnungen am Gurtspanner einpressen..

-   Umlenkrolle in den Gurtspanner einlegen und den Bolzen durchstecken.

![Umlenkrolle]({{site.baseurl}}/assets/images/guide/x-axis/returnpulley.jpg)

![Zusammenbau]({{site.baseurl}}/assets/images/guide/x-axis/construct.jpg)

-   Den fertigen Gurtspanner in den Idler einführen.

-   Selbstsichernde Mutter (M3) einpressen.

![Selbstsichernde Mutter einpressen]({{site.baseurl}}/assets/images/guide/x-axis/selfassuredmother2.jpg)

![Gurtspanner mit zwei Schrauben anschrauben]({{site.baseurl}}/assets/images/guide/x-axis/fixconstruct.jpg)

-   Gurtspanner mit zwei M3x14 Schrauben und Unterlegscheiben anschrauben.

-   Nur so weit, dass der Gurtspanner bündig mit dem Idler ist.

-   Zwei M3x12 Schrauben mit Unterlegscheiben einschrauben (Außen).

-   Lager in den Idler einpressen.

![Lager in Idler einpressen]({{site.baseurl}}/assets/images/guide/x-axis/pressinidler.jpg)

![Lager in Motorhalter einpressen]({{site.baseurl}}/assets/images/guide/x-axis/pressinmotorholder.jpg)

-   Lager in den Motorhalter einpressen.

-   Die Lager mit jeweils M3x10 Schraube und M3-Mutter festschrauben.

![Lager festschrauben]({{site.baseurl}}/assets/images/guide/x-axis/fixbearings.jpg)

![Gewindespindelmutter]({{site.baseurl}}/assets/images/guide/x-axis/threadedspindlenut.jpg)

-   Gewindespindelmutter von den Motoren schraube und in den Idler und den Motorhalter einsetzen.

-   Mit jeweils zwei M3x16 Schrauben sowie 2 normalen Muttern festschrauben.

-   Pulley auf die Welle des Motors stecken und wie abgebildet festschrauben.

-   Eine der beiden Gewindestifte sollte auf der abgeflachten Seite der Motorwelle aufliegen.
-   Auch hier einen kleinen Abstand zwischen Motor und Pulley lassen.

![Pulley]({{site.baseurl}}/assets/images/guide/x-axis/pulley.jpg)

![Motor an Motorhalter]({{site.baseurl}}/assets/images/guide/x-axis/motorfix.jpg)

-   Motor an den Motorhalter mit drei M3x16 Schrauben und Unterlegscheiben festschrauben.

-   Kabel sollte dabei nach unten zeigen.

-   Zwei Gleitmuttern in den Motorhalter einsetzen.

![Gleitmuttern in Motorhalter]({{site.baseurl}}/assets/images/guide/x-axis/motorholderscrews.jpg)

![Kabelführung an Motorhalter]({{site.baseurl}}/assets/images/guide/x-axis/motorholdercable.jpg)

-   Kabelhalter mit einer M3x10 Senkkopfschraube festschrauben und die Gleitmuttern einführen

-   Kabelhalter Deckel auflegen und mit einer M3x12 und einer M3x20 Schraube festziehen.

![Deckel der Kabelführung anbringen]({{site.baseurl}}/assets/images/guide/x-axis/motorholdercablefix.jpg)

![Letzte Schritte]({{site.baseurl}}/assets/images/guide/x-axis/completed.jpg)

-   Die beiden Wellen in den Motorhalter stecken und dann den Extruder vorsichtig aufschieben.
-   Danach den Idler aufstecken.

-   Den Riemen um den Pulley am Motorhalter führen und an der Riemenhalterung vom Extruder befestigen.
-   Die andere Seite des Riemens um die Umlenkrolle im Idler führen und ebenfalls im Extruder befestigen.

![Extruder anbringen]({{site.baseurl}}/assets/images/guide/x-axis/fixextruder.jpg)

Die X-Achse ist somit fertig montiert.
