---
title: Display
layout: home
parent: Anleitung
nav_order: 6

image: /assets/images/cover/6.jpg
---

# Das Display

![Display]({{site.baseurl}}/assets/images/guide/display/completed.jpg)

Dieses Foto zeigt das fertige Display.

* * *

### Was wir für das Display brauchen...

# Bauteile

| Anzahl | Bauteil                |
| :----- | :----------------------|
| 1x     | Display                |
| 2x     | Kabel (IDC)            |
| 2x     | Displayhalter (Winkel) |
| 1x     | Displayrahmen          |


# Schrauben

| Anzahl | Schrauben       |
| :----- | :-------------- |
| 2x     | M3x10           |
| 2x     | M6x12           |
| 2x     | M6 Hammermutter |

![Komponenten]({{site.baseurl}}/assets/images/guide/display/components.jpg)

* * *

### Der Zusammenbau

![Winkel Seiten Display]({{site.baseurl}}/assets/images/guide/display/sides.jpg)

-   Winkel an das Display montieren wie im Bild zu erkennen.
-   Kabel an das Display anbringen.

-   Rahmen an das Display anbringen.
-   Mit zwei M3x10 Schrauben an den Rahmen verschrauben.
-   Display mit zwei M6x12 Schrauben und zwei Hammermuttern an das Profil der Y-Achse anschrauben.

![Aufbauvorschau]({{site.baseurl}}/assets/images/guide/display/completed.jpg)

Das Display ist somit fertig montiert.
