---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults
title: Y-Achse
layout: home
parent: Anleitung
nav_order: 1

image: /assets/images/cover/1.jpg
---

# Die Y-Achse

![Komplette Y Achse]({{site.baseurl}}/assets/images/guide/y-axis/completed.png)

Dieses Foto zeigt die fertige Y-Achse, und damit die Basis.

* * *

### Was wir für die Y-Achse brauchen...

# Bauteile

| Anzahl | Bauteil                      |
| :----- | :----------------------------|
| 2x     | Langes Y-Aluprofil (2 von 3) |
| 2x     | Kurzes Y-Aluprofil           |
| 2x     | Wellen                       |
| 4x     | Winkel                       |
| 4x     | Profilbdeckkappen            |

# Schrauben

| Anzahl | Schrauben       |
| :----- | :-------------- |
| 8x     | M6x14           |
| 8x     | M6 Hammermutter |

![Komponenten]({{site.baseurl}}/assets/images/guide/y-axis/components.png)

* * *

### Der Zusammenbau

![Ausrichtung]({{site.baseurl}}/assets/images/guide/y-axis/construction-alignment.png)

-   Zunächst müssen die Aluprofile wie abgebildet ausgerichtet werden.

-   Es ist wichtig das nicht versehentlich die mittellangen Profile verwendet werden.
    {: .text-red-000 }


-   Dabei darauf achten, dass die langen Aluprofile mit dem offenen Ende nach außen zeigen.

-   Nehmt nun ein kurzes Profil und einen Winkel in die Hand.
-   Eine Schraube (M6x14) durch eine Öffnung des Winkels stecken und leicht in eine Hammermutter schrauben.
-   Die Hammermutter in das Profil stecken.
-   Das selbe auch mit dem kurzen Profil an der anderen Seite durchführen.

![Aufbauvorschau]({{site.baseurl}}/assets/images/guide/y-axis/construction-preview.jpg)

![Anschlagswinkel]({{site.baseurl}}/assets/images/guide/y-axis/90degree.png)

-   Mit einem Anschlagswinkel im 90° Winkel zueinander ausrichten, und festschrauben.

-   Diesen Schritt noch nicht für die anderen beiden Profile wiederholen.

-   Bevor wir das nun mit den anderen Profilen wiederholen müssen wir erst die innenliegenden Winkel aufschieben.

-   Beachtet: Die Winkel müssen in der selben Richtung aufgeschoben werden.
-   Nachdem die Winkel aufgeschoben sind, zeigt der hintere Teil (der Teil des Winkels wo die Schraube verbaut ist) zum hinteren Teil des Druckers.

-   Im Anschluss die beiden letzten Profile montieren.

![Winkel]({{site.baseurl}}/assets/images/guide/y-axis/corner.png)

![Winkel]({{site.baseurl}}/assets/images/guide/y-axis/cap.jpg)

-   Zum Schluss die Profilabdeckkappen aufsetzen.

-   Gegebenenfalls einen Hammer dazu benutzen.

Die Y-Achse ist somit fertig montiert.
