---
title: Z-Achse
layout: home
parent: Anleitung
nav_order: 5

image: /assets/images/cover/5.jpg
---

# Die Z-Achse

![Komplette Z Achse]({{site.baseurl}}/assets/images/guide/z-axis/completed.jpg)

Dieses Foto zeigt die fertige Z-Achse.

* * *

### Was wir für die Z-Achse brauchen...

# Bauteile

| Anzahl | Bauteil                         |
| :----- | :------------------------------ |
| 1x     | Langes Y-Aluprofil (das letzte) |
| 2x     | Mittleres Y-Aluprofil           |
| 2x     | Z - Motoren                     |
| 2x     | Innen-Winkel                    |
| 2x     | Profilbdeckkappen               |
| 2x     | Motor halter                    |
| 2x     | Spindel halter                  |

# Schrauben

| Anzahl | Schrauben       |
| :----- | :-------------- |
| 8x     | M3x10           |
| 4x     | M6x12           |
| 2x     | M6x14           |
| 6x     | M6 Hammermutter |

![Komponenten]({{site.baseurl}}/assets/images/guide/z-axis/components.jpg)

* * *

### Der Zusammenbau

![Ausrichtung]({{site.baseurl}}/assets/images/guide/z-axis/construction-alignment.jpg)

-   Profile mit den Innengelenk-Winkeln verschrauben.

-   Dabei müssen die kurzen Profile seitlich und das lange Profil oberhalb verschraubt werden.

-   Profilabdeckkappen oben an den beiden Enden einfügen.

-   Motorhalter der Z-Achse mit jeweils einer M6x14 und Hammermuttern befestigen.

-   Dieser muss bündig zum Profilende sein und die Öffnungen für die Wellen müssen nach oben zeigen.

![Motorhalter anbringen]({{site.baseurl}}/assets/images/guide/z-axis/motorholder.jpg)

![Wellenhalter anbringen]({{site.baseurl}}/assets/images/guide/z-axis/shaftsupport.jpg)

-   Z-Achsen-Wellenhalter wie dargestellt mit jeweils zwei M6x12 Schrauben festschrauben.

-   Nun die vorbereitete X-Achse zur Hand nehmen.
-   Die Wellen durch die Öffnung im Motorhalter der X-Achse stecken, nachdem es durch die Motorhalter der Z-Achse geführt wurde.
-   Anschließend in den Wellenhalter einführen.

![Zusammenführen]({{site.baseurl}}/assets/images/guide/z-axis/combine.jpg)

![Schrauben anziehen]({{site.baseurl}}/assets/images/guide/z-axis/fixscrew.jpg)

-   Die Einstellschrauben, seitlich vom Idler und dem Motorhalter an der X-Achse, drehen bis diese widerstandslos über die Wellen gleitet.

-   Die Motoren der Z-Achse in die Motorhalter einführen. Die Leitungen durch die Kabelaussparung führen. Danach die Motoren mit M3x10 Schrauben und Unterlegscheiben festschrauben.

-   Dabei vorsichtig und gleichmäßig die spindeln der Motoren von Hand drehen.
-   Der Motor mit dem kurzen Kabel kommt dabei auf die linke Seite, der Motor mit dem langen Kabel auf die rechte Seite.

![Motor anbringen]({{site.baseurl}}/assets/images/guide/z-axis/fixmotor.jpg)

![Schrauben anziehen]({{site.baseurl}}/assets/images/guide/z-axis/combineall.jpg)

-   Die fertige Z-Achse auf die vorbereitete Y-Achsen setzen.

-   Dabei müssen die Motoren mit dem Extruder zusammen nach vorn zeigen.

-   Mithilfe die Lehre ausrichten und festschrauben.

Die Z-Achse ist somit fertig montiert.
