---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults
title: Home
layout: home
nav_order: 1
---

Bauanleitung
============

### für einen 3D Drucker

In dieser Bauanleitung findest du Schritt für Schritt beschrieben, wie du den Bausatz zusammensetzen kannst.

[Start](guide/)

![3D Printer Front]({{site.baseurl}}/assets/images/printer_front.png)
